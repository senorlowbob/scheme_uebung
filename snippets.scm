; Credits to: https://www.youtube.com/watch?v=6k78c8EctXI

(begin
  (display "\n")
  (display "Calculate 2 + 3 and return 5:")
  (display "\n")
  (display (+ 2 3))

  ; Function and Name are separated
  ; Define gives the lambda operation a name
  ; Operation first!
  (define factorial
    (lambda (x)
      (if (< x 1)
        1
        (* x (factorial (- x 1)))
      )
    )
  )

  ; Return 5*4*3*2*1
  (display "Calculate faculty of 5:")
  (display "\n")
  (display (factorial 5))

  ; Define x to be a list of values
  (define x '(1 2 3))
  (display "\n")
  (display (car x)) ; Get first value
  (display "\n")
  (display (cdr x)) ; Get rest of list except first
  (display "\n")
  ; Construct a list from individual values
  (define new_list (cons 1 (cons 2 (cons 3 '()))))
  (display "\n")

  (let*
    (
      (var1 23)
      (var2 45)
      (var3 (+ var1 var2))
    )
    (display "\n")
    (display "Variable var3 is: ")
    (display var3)
  )

  (define double-value
    (lambda (x) (* x 2))
  )
  (display "\n")
  (display (double-value 23))

  (let*
    (
      (double-inside (lambda (x) (* x 2)))
      (current_value 21)
      (result (double-inside current_value))
    )
    (display "\n")
    (display "Doubling yields:")
    (display result)
  )
)
