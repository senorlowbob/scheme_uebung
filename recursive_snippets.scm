; Credits to: https://www.youtube.com/watch?v=6k78c8EctXI

; Solve problem recursively
(letrec
  (
    (eat-apples (lambda (apple-index num-apples)
        (if (>= apple-index num-apples)
          ; Check
          #f ; when exceeded, then do nothing
          (begin
            ; Perform any procedure
            (display "\n")
            (display apple-index)
            ; Narrow
            (eat-apples (+ apple-index 1) num-apples)
          )
        )
      )
    )
  )
  (eat-apples 0 100)
)


(define make-counter
  (lambda (starting-number)
    (let (
          (next-value
            (lambda ()
                (set! starting-number (+ starting-number 1))
                starting-number
            )
          )
        )
        next-value
    )
  )
)

(define first-counter (make-counter 20))
; Attached to different stack frame by deploying same code
(define second-counter (make-counter 200))

(display "\n")
(display "First counter after first call:")
(display (first-counter))
(display "\n")
(display "Second counter after first call:")
(display (second-counter))

(display "\n")
(display "First counter after second call:")
(display (first-counter))
