# README #

Ein paar Scheme-Beispiele aus der Vorlesung:
https://timms.uni-tuebingen.de/tp/UT_20081030_001_info1_0001
werden nachgebaut.

### Quellen ###
- http://www.cs.rpi.edu/academics/courses/fall05/ai/scheme/tips.html#debugging
- https://medium.com/how-to-install/how-to-install-mit-scheme-on-ubuntu-16-0-5809ba2f2a03
- https://stackoverflow.com/questions/903968/how-do-i-execute-a-scm-script-outside-of-the-repl-with-mit-scheme
- https://www.ps.uni-saarland.de/courses/info-i/scheme/doc/user/user_3.html
- http://www.eecs.ucf.edu/~leavens/ui54/WWW/scheme.shtml

### Nutzung ###
Scheme ist eine interpretierte Sprache --> Auf Ubuntu
1. `sudo apt-get install mit-scheme`

2. `mit-scheme --version` oder `scheme --version`

3. `git clone https://senorlowbob@bitbucket.org/senorlowbob/scheme_uebung.git`

4. `scheme < snippets.scm`

### Ersteller ###

* paul.pauuul@gmail.com
